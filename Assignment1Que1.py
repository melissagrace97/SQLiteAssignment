import sqlite3
conn = sqlite3.connect('car.db') #connect to sqlite database
cursor = conn.cursor() #cursor object
cursor.execute("DROP TABLE IF EXISTS CARS") #drop query
cursor.execute('''CREATE TABLE CARS(CAR_NAME CHAR(20) NOT NULL, CAR_OWNER CHAR(20) NOT NULL)''') #create query
records = [('Audi','Melissa'),('Toyota','Sammy'),('Benz','Joe'),('Swift','Ben'),('BMW','Jenny'),('Duster','Melinda'),('Ford','Lissy'),('Jaguar','Teddy'),
('Porsche','Robin'),('Renault','Milan')]
cursor.executemany('''INSERT INTO CARS(CAR_NAME,CAR_OWNER) VALUES(?,?)''', records)
conn.commit()
cursor = conn.execute('SELECT * FROM CARS')
rows = cursor.fetchall()
print("Car Name", "\t Car Owner")
for row in rows:
      print(row[0],"\t \t", row[1], "\n")
conn.close()

