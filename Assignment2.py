import sqlite3
conn = sqlite3.connect('emp.db') #connect to sqlite database
cursor = conn.cursor() #cursor object
cursor.execute("DROP TABLE IF EXISTS Employee") #drop query
#(1a)Create table Employee
cursor.execute('''CREATE TABLE Employee(Name CHAR(20) NOT NULL, Id INT PRIMARY KEY NOT NULL, Salary INT NOT NULL, Department_Id INT NOT NULL )''') #create query
#(1b)Add new column City to Employee Table
addColumn = "ALTER TABLE Employee ADD COLUMN CITY CHAR(20)"
cursor.execute(addColumn)
#(1c)Insert 5 records into Employee Table
employee_records = [('Melissa', '1', '25000', '101', 'Trivandrum'), ('Sammy', '2', '50000','102','Kochi'),
            ('Jose','3','20000','103','Delhi'),('Ben','4','15000','104','Trivandum'),
                    ('Jincy', '5', '45000', '105', 'Kollam')]
cursor.executemany('''INSERT INTO Employee VALUES (?,?,?,?,?)''', employee_records)
conn.commit()
print("Employee Table Created Successfully!! \n") #display the created table
table_display = cursor.execute( ''' SELECT * FROM Employee ''')
table_Records = table_display.fetchall()
print("NAME \t ID \t SALARY \t DEPARTMENT ID \t CITY \n")
for rows in table_Records:
    print(rows[0], "\t", rows[1], "\t", rows[2], "\t", rows[3], "\t", rows[4], "\n")

#(2a)Create Table Departments
cursor.execute('''CREATE TABLE IF NOT EXISTS Departments(Department_Id INT PRIMARY KEY NOT NULL, Department_Name CHAR(20) NOT NULL)''') #create query
#(2b)Insert 5 records into Department Table
dept_records = [('101', 'R&D'), ('102', 'Marketing'), ('103', 'Production'), ('104', 'HR'), ('105','Accounting')]
cursor.executemany('''INSERT OR IGNORE INTO Departments VALUES(?,?)''', dept_records)
conn.commit()
print("Department Table Created Successfully!! \n") #display the created table
table_display = cursor.execute( ''' SELECT * FROM Departments ''')
table_Records = table_display.fetchall()
print("Department_ID \t Department_Name\n")
for rows in table_Records:
    print(rows[0], "\t", rows[1],"\n")
cursor.close()

def get_connection():
    conn = sqlite3.connect('emp.db')
    return conn
def close_connection(conn):
    if conn:
        conn.close()

def get_details(): #(1d) Display Name,Id and Salary from Employee Table
    conn = get_connection()
    c = conn.cursor()
    get_emp = """ SELECT Name,Id,Salary from Employee """
    c.execute(get_emp)
    records = c.fetchall()
    print("NAME \t ID \t SALARY \n")
    for row in records:
        print(row[0], "\t", row[1], "\t", row[2], "\n")
    close_connection(conn)

def get_employee_letter(ch): #(1e)Display details of employees starting with a letter input by the user
    conn = get_connection()
    cursor = conn.cursor()
    cursor.execute(" SELECT * FROM Employee where Name LIKE ? || '%' ORDER BY Name ", ch)
    records = cursor.fetchall()
    print("Details of employees whose names start with ", ch)
    for row in records:
        print("Name:", row[0])
        print("Id:", row[1])
        print("Salary:", row[2])
        print("Department Id:", row[3])
        print("City:", row[4])
    close_connection(conn)

def get_employeeid_details(Id): #(1f)Display details of employees with IDs input by the user
    conn = get_connection()
    cursor = conn.cursor()
    select_query1 = """ SELECT * FROM Employee where Id = ? """
    cursor.execute(select_query1, (Id,))
    records = cursor.fetchall()
    print("Employee details of Id  ", Id)
    for row in records:
        print("Name:", row[0])
        print("Id:", row[1])
        print("Salary:", row[2])
        print("Department Id:", row[3])
        print("City:", row[4])
    close_connection(conn)

def change_emp_name(Id, updated_name): #(1g)Change the name of the employee whose Id is input by the user
    conn = get_connection()
    cursor = conn.cursor()
    update_query = " UPDATE Employee SET Name = ? WHERE Id = ? "
    data = (updated_name, Id)
    cursor.execute(update_query, data)
    conn.commit()
    print("Updated table\n")
    updated_table_display = cursor.execute(''' SELECT * FROM Employee ''')
    updated_record = updated_table_display.fetchall()
    print("NAME \t ID \t SALARY \t DEPARTMENT ID \t CITY \n")
    for row in updated_record:
        print(row[0], "\t", row[1], "\t", row[2], "\t", row[3], "\t", row[4], "\n")
    conn.commit()
    close_connection(conn)

def get_department(Department_Id):
    conn = get_connection()
    cursor = conn.cursor()
    select_query = """ SELECT * FROM Departments where Department_Id = ?"""
    cursor.execute(select_query, (Department_Id,))
    record = cursor.fetchone()
    close_connection(conn)
    return record[1]

def get_employees(Department_Id): #(2c) Details of employees of a particular department
    Department_Name = get_department(Department_Id)
    conn = get_connection()
    cursor = conn.cursor()
    select_query1 = """ SELECT * FROM Employee where Department_Id = ? """
    cursor.execute(select_query1, (Department_Id,))
    records = cursor.fetchall()
    print("Employees of ", Department_Name)
    for row in records:
        print("Employee Name:", row[0])
        print("Employee ID:", row[1])
        print("Salary:", row[2])
        print("Department Name:", Department_Name)
        print("Department ID:", row[3])
        print("Employee City:", row[4], "\n")
    close_connection(conn)

print("******Get name, id and salary from employee table******\n")
get_details()
print("******Details of employees whose names start with a particular letter******\n")
while(True):
    ch = input("Enter the starting letter to get the details of the employees:")
    get_employee_letter(ch)
    check_again = input('Would you like to view employee details starting with a different letter [y/n]? \n')
    if check_again.lower() == 'n':
        break
print("******Details of employees as per given Id******\n")
while(True):
    Id = input("Enter the Id of the employee to view the details:")
    get_employeeid_details(Id)
    check_again = input('Would you like to view another employee details [y/n]? \n')
    if check_again.lower() == 'n':
        break
print("******Change name of employee whose ID is input by the user******\n")
while(True):
    Id = input("Enter the Id of the employee whose name is to be changed:")
    updated_name = input("Enter the updated name:")
    change_emp_name(Id, updated_name)
    change_again = input('Would you like to change the name of another employee [y/n]? \n')
    if change_again.lower() == 'n':
        break
print("******Details of Employees of a particular Department******\n")
while(True):
    Department_Id = input("Enter the Department Id:")
    get_employees(Department_Id)
    check_again = input('Would you like to view another department details [y/n]? \n')
    if check_again.lower() == 'n':
        break