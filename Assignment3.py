import sqlite3
import pandas as pd
conn = sqlite3.connect("database.sqlite") #connect to sqlite database
cursor = conn.cursor() #cursor object

#Que:1b-1e
#Que: 1b
print("\nQue:1b Print the names of both the Home Teams and Away Teams in each match played in 2015 and Full time Home Goals (FTHG) = 5 \n")
cursor.execute(('''select HomeTeam,AwayTeam from Matches where Season = 2015 and FTHG = 5'''))
data = pd.DataFrame(cursor.fetchall())
data.columns = [x[0] for x in cursor.description]
print(data)
conn.commit()

#Que: 1c
print("\nQue:1c Print the details of the matches where Arsenal is the Home Team and  Full Time Result (FTR) is “A” (Away Win)\n")
cursor.execute('''select * from "Matches" where "HomeTeam" = "Arsenal" and FTR = "A"''')
data = pd.DataFrame(cursor.fetchall())
data.columns = [x[0] for x in cursor.description]
print(data)
conn.commit()

#Que:1d
print("\nQue:1d Print all the matches from the 2012 season till the 2015 season where Away Team is Bayern Munich and Full time Away Goals (FTHG) > 2 \n")
cursor.execute('''select * from "Matches" where "Season" between 2012 and 2015  and "AwayTeam" = "Bayern Munich" and FTHG > 2''')
data = pd.DataFrame(cursor.fetchall())
data.columns = [x[0] for x in cursor.description]
print(data)
conn.commit()

#Que: 1e
print("\nQue:1e Print all the matches where the Home Team name begins with “A” and Away Team name begins with “M” \n")
cursor.execute('''select * from "Matches" where "HomeTeam" like 'A%' and "AwayTeam" like 'M%' ''')
data = pd.DataFrame(cursor.fetchall())
data.columns = [x[0] for x in cursor.description]
print(data)
conn.commit()

#Que:2a-2e
#Que:2a
print("\nQue:2a Counts all the rows in the Teams table\n")
cursor.execute('''select Count (*) from "Teams" ''')
data = pd.DataFrame(cursor.fetchall())
data.columns = [x[0] for x in cursor.description]
print(data)
conn.commit()

#Que:2b
print("\nQue:2b Print all the unique values that are included in the Season column in the Teams table\n")
cursor.execute('''select distinct Season from "Teams"''')
data = pd.DataFrame(cursor.fetchall())
data.columns = [x[0] for x in cursor.description]
print(data)
conn.commit()

#Que:2c
print("\nQue:2c Print the largest and smallest stadium capacity that is included in the Teams table\n")
cursor.execute('''select max(StadiumCapacity) as largest_StadiumCapacity, min(StadiumCapacity) as smallest_StadiumCapacity from "Teams"''')
data = pd.DataFrame(cursor.fetchall())
data.columns = [x[0] for x in cursor.description]
print(data)
conn.commit()

#Que:2d
print("\nQue:2d Print the sum of squad players for all teams during the 2014 season from the Teams table [Answer - 1164]\n")
cursor.execute('''select sum(KaderHome) as Squad_Player from "Teams" where Season = 2014''')
data = pd.DataFrame(cursor.fetchall())
data.columns = [x[0] for x in cursor.description]
print(data)
conn.commit()

#Que:2e
print("\nQue:2e Query the Matches table to know how many goals did Man United score during home games on average? [Answer - 2.16]\n")
cursor.execute('''select round(avg(FTHG),2) as home_goals from "Matches" where "HomeTeam" = "Man United"''')
data = pd.DataFrame(cursor.fetchall())
data.columns = [x[0] for x in cursor.description]
print(data)
conn.commit()

#Que:3a-3g
#Que:3a
print("\nQue:3a Write a query that returns the HomeTeam, FTHG (number of home goals scored in a game) "
      "and FTAG (number of away goals scored in a game) from the Matches table. "
      "Only include data from the 2010 season and where ‘Aachen’ is the name of the home team."
      " Return the results by the number of home goals scored in a game in descending order. \n")
cursor.execute('''select "HomeTeam",FTHG, FTAG from "Matches" where "HomeTeam" = "Aachen" and "Season" = 2010 order by FTHG desc ''')
data = pd.DataFrame(cursor.fetchall())
data.columns = [x[0] for x in cursor.description]
print(data)
conn.commit()

#Que:3b
print("\nQue:3b Print the total number of home games each team won during the 2016 season"
      " in descending order of number of home games from the Matches table.\n")
cursor.execute('''select "HomeTeam", count (FTR) as total_home_wins from "Matches" where "Season" = 2016 and FTR = 'H' group by "HomeTeam" order by count(FTR) desc ''')
data = pd.DataFrame(cursor.fetchall())
data.columns = [x[0] for x in cursor.description]
print(data)
conn.commit()

#Que:3c
print("\nQue:3c Write a query that returns the first ten rows from the Unique_Teams table\n")
cursor.execute('''select * from "Unique_Teams" limit 10''')
data = pd.DataFrame(cursor.fetchall())
data.columns = [x[0] for x in cursor.description]
print(data)
conn.commit()

#Que:3d Using WHERE
print("\nQue:3d Print the Match_ID and Unique_Team_ID with the corresponding Team_Name from the Unique_Teams and Teams_in_Matches tables. Use the WHERE statement first and then use the JOIN statement to get the same result. \n")
cursor.execute('''select * from "Teams_in_Matches","Unique_Teams" where "Teams_in_Matches"."Unique_Team_ID" = "Unique_Teams"."Unique_Team_ID"''')
data = pd.DataFrame(cursor.fetchall())
data.columns = [x[0] for x in cursor.description]
print(data)
conn.commit()

#Que:3d Using JOIN
print("\nQue:3d Print the Match_ID and Unique_Team_ID with the corresponding Team_Name from the Unique_Teams and Teams_in_Matches tables. Use the WHERE statement first and then use the JOIN statement to get the same result. \n")
cursor.execute('''select * from "Teams_in_Matches" join "Unique_Teams" on "Teams_in_Matches"."Unique_Team_ID" = "Unique_Teams"."Unique_Team_ID"''')
data = pd.DataFrame(cursor.fetchall())
data.columns = [x[0] for x in cursor.description]
print(data)
conn.commit()

#Que:3e
print("\nQue:3e Write a query that joins together the Unique_Teams data table and the Teams table, and returns the first 10 rows.\n")
cursor.execute('''select * from "Unique_Teams" join "Teams" on "Unique_Teams"."TeamName"="Teams"."TeamName" limit 10''')
data = pd.DataFrame(cursor.fetchall())
data.columns = [x[0] for x in cursor.description]
print(data)
conn.commit()

#Que:3f
print("\nQue:3f Write a query that shows the Unique_Team_ID and TeamName from the Unique_Teams table and AvgAgeHome, Season and ForeignPlayersHome from the Teams table. Only return the first five rows.\n")
cursor.execute('''select "Unique_Teams"."Unique_Team_ID", "Unique_Teams"."TeamName", "Teams"."AvgAgeHome", "Teams"."Season" , "Teams"."ForeignPlayersHome" from "Unique_Teams" join "Teams" on "Unique_Teams"."TeamName"="Teams"."TeamName" limit 5''')
data = pd.DataFrame(cursor.fetchall())
data.columns = [x[0] for x in cursor.description]
print(data)
conn.commit()

#Que:3g
print("\nQue:3g Write a query that shows the highest Match_ID for each team that ends in a “y” or a “r”. Along with the maximum Match_ID, display the Unique_Team_ID from the Teams_in_Matches table and the TeamName from the Unique_Teams table.\n")
cursor.execute('''select max(Match_ID),"Teams_in_Matches"."Unique_Team_ID",TeamName from "Teams_in_Matches" join "Unique_Teams" on "Teams_in_Matches"."Unique_Team_ID" = "Unique_Teams"."Unique_Team_ID"
where "TeamName" like 'y%' or "TeamName" like 'r%' group by "Teams_in_Matches"."Unique_Team_ID","TeamName"''')
data = pd.DataFrame(cursor.fetchall())
data.columns = [x[0] for x in cursor.description]
print(data)
conn.commit()

conn.close()

