import sqlite3

conn = sqlite3.connect('hospital.db')
cursor = conn.cursor()
cursor.execute(''' CREATE TABLE IF NOT EXISTS Hospital( Hospital_Id INT PRIMARY KEY NOT NULL,
                        Hospital_Name NOT NULL, Bed_Count INT NOT NULL) ''')
hospital_records = [('1', 'Mayo Clinic', '200'), ('2', 'Cleveland Clinic', '400'), ('3', 'Johns Hopkins', '1000'),
                    ('4', 'UCLA Medical Center', '1500')]
cursor.executemany(''' INSERT OR IGNORE INTO Hospital (Hospital_Id, Hospital_Name, Bed_Count)  VALUES(?,?,?)''', hospital_records)
cursor.execute(''' CREATE TABLE IF NOT EXISTS Doctor( Doctor_Id INT PRIMARY KEY NOT NULL,
                        Doctor_Name NOT NULL, Hospital_Id INT NOT NULL, Joining_Date NOT NULL, Speciality NOT NULL, 
                        Salary INT NOT NULL, Experience INT NULL) ''')
doctor_records = [('101', 'David', '1', '2005-02-10', 'Pediatric', '40000', 'NULL'),
                  ('102', 'Michael', '1', '2018-07-23', 'Oncologist', '20000', 'NULL'),
                  ('103', 'Susan', '2', '2016-05-19', 'Garnacologist', '25000', 'NULL'),
                  ('104', 'Robert', '2', '2017-12-28', 'Pediatric', '28000', 'NULL'),
                  ('105', 'Linda', '3', '2004-06-14', 'Garnacologist', '42000', 'NULL'),
                  ('106', 'William', '3', '2012-09-11', 'Dermatologist', '30000','NULL'),
                  ('107', 'Richard', '4', '2014-08-21', 'Garnacologist', '32000', 'NULL'),
                  ('108', 'Karen', '4', '2011-10-17', 'Radiologist', '30000', 'NULL')]
cursor.executemany('''INSERT OR IGNORE INTO Doctor VALUES(?,?,?,?,?,?,?)''', doctor_records)
conn.commit()
print("Hospital Table Created\n")  #display the created table
table_display = cursor.execute( ''' SELECT * FROM Hospital ''')
table_Records = table_display.fetchall()
print("HOSPITAL_ID \t HOSPITAL_NAME \t BED_COUNT\n")
for rows in table_Records:
    print(rows[0], "\t", rows[1], "\t", rows[2], "\n")

print("Doctor Table Created\n")
table_display = cursor.execute( ''' SELECT * FROM Doctor ''')
table_Records = table_display.fetchall()
print("DOCTOR_ID \t NAME \t HOSPITAL_ID \t JOINING_DATE \t SPECIALITY \t SALARY \t EXPERIENCE \n")
for rows in table_Records:
    print(rows[0], "\t", rows[1], "\t", rows[2], "\t", rows[3], "\t", rows[4], rows[5], rows[6], "\n")
cursor.close()

def get_connection():
    conn = sqlite3.connect('hospital.db')
    return conn
def close_connection(conn):
    if conn:
        conn.close()

def doctors_list(speciality, salary):
    conn = get_connection()
    c = conn.cursor()
    get_doctor = """SELECT * FROM Doctor where Speciality = ? and Salary > ?"""
    c.execute(get_doctor, (speciality, salary))
    records = c.fetchall()
    print("Doctors whose specialty is", speciality, "and salary greater than", salary, "\n")
    for row in records:
        print("Doctor Id: ", row[0])
        print("Doctor Name:", row[1])
        print("Hospital Id:", row[2])
        print("Joining Date:", row[3])
        print("Specialty:", row[4])
        print("Salary:", row[5])
        print("Experience:", row[6], "\n")
    close_connection(conn)

def get_hospital_name(hospital_id):
    conn = get_connection()
    cursor = conn.cursor()
    select_query = """ SELECT * FROM Hospital where Hospital_Id = ?"""
    cursor.execute(select_query, (hospital_id,))
    record = cursor.fetchone()
    close_connection(conn)
    return record[1]

def get_doctors(hospital_id):
    hospital_name = get_hospital_name(hospital_id)
    conn = get_connection()
    cursor = conn.cursor()
    select_query1 = """ SELECT * FROM Doctor where Hospital_Id = ? """
    cursor.execute(select_query1, (hospital_id,))
    records = cursor.fetchall()
    print("Doctors of ", hospital_name)
    for row in records:
        print("Doctor Id:", row[0])
        print("Doctor Name:", row[1])
        print("Hospital Id:", row[2])
        print("Hospital Name:", hospital_name)
        print("Joining Date:", row[3])
        print("Specialty:", row[4])
        print("Salary:", row[5])
        print("Experience:", row[6], "\n")
    close_connection(conn)

print("List of Doctors as per Speciality and Salary\n")
while(True):
    speciality = input("Enter the Speciality of the Doctor:")
    salary = input("Enter the Salary:")
    doctors_list(speciality, salary)
    check_again = input('Would you like to view another doctor details [y/n]? \n')
    if check_again.lower() == 'n':
        break
print("List of Doctors as per given Hospital Id\n")
while(True):
    hospital_id = input("Enter the Id of the hospital to view the doctor details:")
    get_doctors(hospital_id)
    check_again = input('Would you like to view another doctor details [y/n]? \n')
    if check_again.lower() == 'n':
        break
